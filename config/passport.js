var LocalStrategy = require('passport-local').Strategy;

var User = require('../app/models/user');

module.exports = function(passport) {

    ///////////////////////////////////
    // serializing and deserializing //
    ///////////////////////////////////
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    //////////////////
    // LOCAL SIGNUP //
    //////////////////
    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // pass the request to callback
        },
        function(req, email, password, done) {

            // asynchronous
            // User.findOne does not fire until data is sent back
            User.findOne({ 'local.email': email }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {
                    return done(null, false, req.flash('signupMessage', 'Email already taken'));
                }
                else {
                    // create new user
                    var newUser = new User();

                    // set user's credentials
                    newUser.local.email = email;
                    newUser.local.password = newUser.generateHash(password);

                    // save
                    newUser.save(function(err) {
                        if (err)
                            return done(null, false, req.flash('signupMessage', 'Something went wrong'));
                        else
                            return done(null, newUser);
                    });
                }
            });
        }));

    /////////////////
    // LOCAL LOGIN //
    /////////////////
    passport.use('local-login', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // pass the request to callback
        },
        function(req, email, password, done) {

            User.findOne({ 'local.email': email }, function(err, user) {
                if (err)
                    return done(err);

                // no user found
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found'));

                // check password
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password'));

                // all good
                return done(null, user);
            });
        }));
};
