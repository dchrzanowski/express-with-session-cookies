module.exports = function(app, passport) {
    //////////
    // root //
    //////////
    app.get('/', function(req, res) {
        res.render('index.ejs');
    });

    ///////////
    // login //
    ///////////
    app.get('/login', function(req, res) {
        res.render('login.ejs', {
            msg: req.flash('loginMessage')
        });
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.post('/login-mobile', function(req, res, next) {
        passport.authenticate('local-login', function(err, user) {
            if (err)
                return res.status(502).json({err: err});
            if (!user)
                return res.status(502).json({err: err});

            req.logIn(user, function() {
                res.json({user: user});
            });
        })(req, res, next);
    });
    ////////////
    // signup //
    ////////////
    app.get('/signup', function(req, res) {
        res.render('signup.ejs', {
            msg: req.flash('signupMessage')
        });
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    app.post('/signup-mobile', function(req, res, next) {
        passport.authenticate('local-signup', function(err, user) {
            if (err)
                return res.status(502).json({err: err});
            if (!user)
                return res.status(502).json({err: err});

            req.logIn(user, function() {
                res.json({user: user});
            });
        })(req, res, next);
    });

    /////////////
    // profile //
    /////////////
    app.get('/profile', isLoggedIn(), function(req, res) {
        res.render('profile.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    app.get('/profile-mobile', isLoggedIn(true), function(req, res) {
        res.json({
            user: req.user
        });
    });

    app.get('/profile-mobile-throw', function(req, res) {
        throw 'Puta';
    });

    app.get('/mobile-info', function(req, res) {
        res.json({
            hello: 'world'
        });
    });

    ////////////
    // logout //
    ////////////
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    ///////////////////////////////
    // check if user is logged in //
    ///////////////////////////////
    function isLoggedIn(isMobile) {
        return function(req, res, next) {
            if (req.isAuthenticated()) {
                return next();
            }

            if (isMobile)
                return res.status(504).json({err: 'Session expired'});
            return res.redirect('/login');
        };
    }
};
