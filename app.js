var express      = require('express');
var path         = require('path');
var app          = express();
var port         = 8080;
var mongoose     = require('mongoose');
var passport     = require('passport');
var flash        = require('connect-flash');
var cors            = require("cors");

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var MongoStore   = require('connect-mongo')(session);

// setup mongo
mongoose.connect('mongodb://db:27017/ExpressSessionTest');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongo connection error:'));
db.once('open', function (callback) {
    console.log("Connected to mongoDB");
});

// allow cors
app.use(cors());

// serve static files
app.use(express.static(path.join(__dirname, '/public/')));

// passport setup
require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms
app.set('view engine', 'ejs'); // setup view engine

// session and session store setup
app.use(session({
    secret: 'kwueicobhanscoaewr3498aovonc9nij',
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// 'before anything else' middleware
app.use(function(req, res, next) {
    console.log("---- Cookies ----\n", req.cookies);
    next();
});

// setup routes
require('./app/routes.js')(app, passport);

app.listen(port, function() {
    console.log('Express running on: ' + port);
});
